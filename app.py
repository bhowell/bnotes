#!/usr/bin/env python3
# Copyright 2016 Brendan Howell.
#
# This file is part of BNotes.
#
#  BNotes is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  BNotes is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with BNotes. If not, see <http://www.gnu.org/licenses/>.

import configparser
import datetime
import os

import bottle
import blitzdb


class Note(blitzdb.Document):
    pass


class Tag(blitzdb.Document):
    pass

config = configparser.ConfigParser()
config.read("bnotes.cfg")
TEMPLATE = config["SITE"]["template"]
PORT = config["SITE"]["port"]

db = blitzdb.FileBackend("./data")

app = bottle.app()

viewpath = os.path.join("views", TEMPLATE)
bottle.TEMPLATE_PATH.append(viewpath)
staticpath = os.path.join("views", TEMPLATE, "static")


def get_note(noteid):
    note = db.get(Note, {'pk': noteid})
    # TODO: handle Note.DoesNotExist exception and
    # Note.MultipleDocumentsReturned
    return note


def edit_note(noteid=None, title="Untitled Note", content=" ", newtags=[]):
    if noteid:
        note = get_note(noteid)
    else:
        note = Note({"tags": []})
        note.created = str(datetime.datetime.now())

    note.title = title
    note.content = content
    note.updated = str(datetime.datetime.now())

    # delete removed tag refs
    for t in range(len(note.tags)):
        tag = note.tags[t]
        if tag.name not in newtags:
            # remove refs
            del_tag_note(tag, note)
            del(note.tags[t])

    for tagname in newtags:
        tag = create_tag(tagname)
        if tag not in note.tags:
            note.tags.append(tag)
        if note not in tag.notes:
            tag.notes.append(note)
            db.save(tag)

    db.save(note)
    db.commit()
    return


def del_tag_note(tag, note):
    for n in len(tag.notes):
        if tag.notes[n] == note:
            del(tag.notes[n])
            db.save(tag)
            return
    return


def create_tag(tag_text):
    try:
        tag = db.get(Tag, {'name': tag_text})
    except Tag.DoesNotExist:
        tag = Tag({"name": tag_text, "notes": []})
        db.save(tag)
        db.commit()
    return tag


def delete_note(noteid):
    note = get_note(noteid)
    db.delete(note)
    db.commit()


def update_note(noteid, content, title, tags):
    note = get_note(noteid)
    note.content = content
    note.title = title
    note.tags = []
    note.updated = str(datetime.datetime.now())
    for tag in tags:
        ntag = create_tag(tag)
        note.tags.append(ntag)
        if note not in ntag.notes:
            ntag.notes.append(note)
            db.save(ntag)
    db.save(note)
    db.commit()


def get_notes():
    notes = db.filter(Note, {}).sort(key="created",
                                     order=blitzdb.queryset.QuerySet.DESCENDING)
    return notes


def get_tags():
    tags = db.filter(Tag, {}).sort(key="name")
    return tags


@bottle.get("/")
@bottle.route("/tag/<tagid>")
@bottle.route("/note/<noteid>")
def index(tagid=None, noteid=None):
    getd = bottle.request.query
    if hasattr(getd, "q"):
        print("q:", getd.q)
        q = getd.q
        # TODO: search
    else:
        q = None

    if noteid:
        note = get_note(noteid)
    else:
        note = None

    if tagid:
        tag = db.get(Tag, {"pk": tagid})
        notes = tag.notes
    else:
        notes = get_notes()
        tag = None

    tags = get_tags()
    return bottle.template("index", note=note, notes=notes, tags=tags, tag=tag,
                           q=q)


# @bottle.route("/note/:noteid")
def note(noteid):
    note = get_note(noteid)
    notes = get_notes()
    if note:
        return bottle.template("note", note=note, notes=notes)
    else:
        bottle.abort(404, "I still haven't found what you're looking for.")

    return bottle.template("edit", note=note)


@bottle.post("/edit")
def edit():
    postd = bottle.request.forms
    if postd.noteid == "":
        noteid = None
    else:
        noteid = postd.noteid
    content = postd.notecontent
    title = postd.title

    tagstring = postd.tags.split(",")
    tags = []
    for tag in tagstring:
        tag = tag.strip()
        tags.append(tag)

    print("editing id:%s title: %s \n %s\ntags: %s" % (noteid, title, content, tags))
    edit_note(noteid, title, content, tags)
    bottle.redirect("/")


@bottle.route("/admin/del/:noteid")
def delete(noteid):
    delete_note(noteid)
    bottle.redirect("/admin")


@bottle.route("/static/<filename:path>")
def server_static(filename):
    # print staticpath + "/" + filename
    return bottle.static_file(filename, root=staticpath)


@bottle.route("/js/<filename:path>")
def server_static_js(filename):
    return bottle.static_file(filename, root="js")


def main():
    bottle.run(app=app, quiet=False, reloader=True, port=PORT)

if __name__ == "__main__":
    main()
