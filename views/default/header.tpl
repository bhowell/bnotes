<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<link rel="stylesheet" media="screen" href="http://openfontlibrary.org/face/station" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/static/style.css" type="text/css" />
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<script src="/static/ui.js"></script>
</head>
<body>
<div id="main">
  <div id="header">
    <div id="title"><a href="/">BNotes</a></div>
  </div>
  <div class="clear"></div>
