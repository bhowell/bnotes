%include("default/header.tpl")
<div id="content">
  <div id="menu">
  <ul>
    <li>&nbsp;</li>
  </ul>
  </div>
  <div id="ctext">
       <h4>Site Administration</h4>
       <table id="noteadmin">
         <caption>Notes</caption>
    %for note in notes:
         <tr>
           <td>{{note[0]}}</td>
           <td><a href="/admin/del/{{note[0]}}">Delete</a></td>
           <td><a href="/admin/e/{{note[0]}}">Edit</a></td>
         </tr>  
    %end
       </table>

    <br />   
    <form action="/admin/new" method="POST">
      New Note: <input type="text" name="noteid" size="30" />
    <input type="submit" value="Create" />
    </form>
  </div>
</div>

%include("default/footer.tpl")
