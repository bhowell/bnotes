%include("default/header.tpl")
<div id="content">
  <div id="menu">
  <ul>
    <li><a href="/admin">Site Admin</a></li>
    <br /><br /><br /><br />
    <h6>click on the text here to start editing -></h6>
  </ul>
  </div>
  <div id="ctext">
    <form id="editform" action="/admin/save" method="POST">
    %if note[0] == "":
       <h4>Create Page</h4>
       Name: <input type="text" size="30" name="noteid" />
    %else:
       <h4>Edit Page: {{note[0]}}</h4>
       <input type="hidden" name="noteid" value="{{note[0]}}" />
       <input type="hidden" name="notecontent" id="contentfield" value="" />
       <div id="noteContents">{{!note[1]}}</div>
    %end
    <input type="submit" value="Save" id="saveedit" />
    </form>
  </div>
</div>
<link href="/js/alloy-editor/assets/alloy-editor-ocean-min.css" rel="stylesheet">
<script src="/js/alloy-editor/alloy-editor-all-min.js"></script>
<script>
  AlloyEditor.editable('noteContents');

  var button = {
    clicked: false,

    click: function(){
      this.clicked = true;
      contents = document.getElementById("noteContents").innerHTML;
      field = document.getElementById("contentfield");
      field.value = contents;
    }
  };

  btn = document.getElementById("saveedit");
  btn.addEventListener("click", button.click, false);
  
</script>

%include("default/footer.tpl")
