%include("default/header.tpl")
%if note:
<div id="newform" style="display: block">
%else:
<div id="newform">
%end
    {{"New Note:" if note is None else ""}}
      <div class="closenote" id="closenotebtn"><a href="/" title="close">✖</a></div>
    %if note:
      <div class="closenote" id="editbtn"><a href="#" title="edit">✎</a></div> 
    %end
    <form method="POST" action="/edit">
      <input type="hidden" name="noteid" value="{{note.pk if note else ""}}" />
      <input type="hidden" name="notecontent" id="contentfield" value="" />
      <div id="noteTitle">{{note.title if note else "Note Title"}}</div>
      <input type="hidden" name="title" id="noteTitleField" />
      <div id="noteContents">{{!note.content if note else "Click here to start editing."}}</div>
      <div id="noteTags">
      %if note:
          %for tag in note.tags:
            <a class="tag" href="/tag/{{tag.pk}}">{{tag.name}}</a>&nbsp;
          %end
      %end
      </div>
      <input type="text" name="tags" id="tagsfield" style="display: none" size="40" value="{{", ".join([t.name for t in note.tags]) if note else ""}}" placeholder="Enter tags separated by ','" />
      <input type="submit" value="Save" id="saveedit" />
    </form>
</div>
%if note:
<div id="notes" style="display: none">
%else:
<div id="notes">
%end
  %if tag:
  <div id="filtermsg">Notes with Tag: {{tag.name}}</div>
  %end
  %if q:
  <div id="filtermsg">Notes matching query: {{q}}</div>
  %end
  <div><input type="button" value="+" id="addnotebtn" /></div>
  <ul>
  %for n in notes:
    <li><a href="/note/{{n.pk}}">{{n.title}}</a></li>
  %end
  </ul>
</div>
<div id="filter">
  <div id="search">
    <form method="GET" action="/">
      <input type="text" name="q" size="30" placeholder="Search terms" />
      <input type="submit" value="Search" />
    </form>
  </div>
  <div id="tags">
  <span id="tagstitle">Tags</span> 
  %for tag in tags:
    <a class="tag" href="/tag/{{tag.pk}}">{{tag.name}}</a>&nbsp;
  %end
  </div>
</div>
<link href="/js/alloy-editor/assets/alloy-editor-ocean-min.css" rel="stylesheet">
<script src="/js/alloy-editor/alloy-editor-all-min.js"></script>
<script>
  // todo: turn this on with a button and disable with a reload?
  var editbutton = {
    clicked: false,
    click: function(){
      console.log("edit button clicked");
      this.clicked = true;
      editor = AlloyEditor.editable('noteContents');
      document.getElementById("noteTitle").contentEditable = "true";
      document.getElementById("noteTags").style.display = "none";
      document.getElementById("tagsfield").style.display = "block";
      // for each .tag grab click events
      taglist = document.getElementsByClassName("tag");
      for(var i=0; i < taglist.length; i++){
        console.log("adding listener for ", taglist[i]);
        taglist[i].addEventListener("click", function(evnt){
          evnt.stopPropagation();
          evnt.preventDefault();
          tagsfield = document.getElementById("tagsfield");
          if(tagsfield.value.length > 0){
            tagsfield.value += ', ' + this.innerHTML;
          }
          else{
            tagsfield.value = this.innerHTML;
          }
          console.log(tagsfield);
        }, false);
      }
    }
  };

  %if note:
  console.log("Note: {{note}}");
  editbtn = document.getElementById("editbtn");
  editbtn.addEventListener("click", editbutton.click, false);
  //%else:
  //editor = AlloyEditor.editable('noteContents');
  //console.log("opening new entry");
  //editbutton.click();
  %end

  var button = {
    clicked: false,

    click: function(){
      this.clicked = true;
      contents = document.getElementById("noteContents").innerHTML;
      field = document.getElementById("contentfield");
      field.value = contents;
      title = document.getElementById("noteTitle").innerHTML.replace(/(<([^>]+)>)/ig,"");
      titlefield = document.getElementById("noteTitleField");
      titlefield.value = title;
    }
  };

  btn = document.getElementById("saveedit");
  btn.addEventListener("click", button.click, false);

  addnote = {
    clicked: false,
    
    click: function(){
      this.clicked = true;
      notes = document.getElementById("notes");
      notes.style.display = "none";
      newform = document.getElementById("newform");
      newform.style.display = "block";
      editbutton.click();
    }
  };

  addnotebtn = document.getElementById("addnotebtn");
  addnotebtn.addEventListener("click", addnote.click, false);
</script>
%include("default/footer.tpl")
